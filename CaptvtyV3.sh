#!/bin/bash

#Mise à jour du système :
sudo pacman -Syyu && yay -Syua
#Installation des paquets nécesssaires
sudo pacman -S winetricks lib32-gnutls lib32-libldap icoutils
yay -S ttf-ms-fonts

#Nettoyage
[ -d "$HOME/.wine_captvty_V3" ] && rm -rf "$HOME/.wine_captvty_V3"
[ -d "$HOME/.captvty_V3/" ] && rm -rf "$HOME/.captvty_V3/"
[ -f "$HOME/.local/share/applications/Captvty_V3.desktop" ] && rm "$HOME/.local/share/applications/Captvty_V3.desktop"
[ -f "$HOME/.icons/captvty-logo.png" ] && rm "$HOME/.icons/captvty-logo.png"

#Ajout sur le prefix des éléments nécessaires à Captvty
export WINEPREFIX="$HOME/.wine_captvty_V3"
export WINEARCH=win32
wineboot -u
wineboot --init

#Récupération de la dernière version de winetricks
wget -P /home/$USER/Téléchargements https://github.com/Winetricks/winetricks/archive/master.zip
unzip /home/$USER/Téléchargements/master.zip -d /home/$USER/Téléchargements
sudo mv /usr/bin/winetricks /usr/bin/winetricks.old
sudo cp /home/$USER/Téléchargements/winetricks-master/src/winetricks /usr/bin

#Installation des dépendances avec Winetricks
/usr/bin/winetricks dotnet452
/usr/bin/winetricks gdiplus
/usr/bin/winetricks vlc
/usr/bin/winetricks comctl32
/usr/bin/winetricks --force dotnet452

#Récupération du programme proprement dit
adresse=$(wget -q -O- 'http://v3.captvty.fr' | sed -n 's/.*href="\(\/\/.\+\.zip\).*/http:\1/p')
test -n "$adresse" && wget -qO /tmp/Captvty.zip "$adresse"
if test -n /tmp/Captvty.zip
then
   unzip -d "$HOME/.captvty_V3/" /tmp/Captvty.zip &&  rm /tmp/Captvty.zip
fi

#Récupération de l'icône
test -d "$HOME/.icons" || mkdir "$HOME/.icons"
wrestool -x --output="$HOME/.icons/captvty-logo.png"  -t14 "$HOME/.captvty_V3/Captvty.exe"

#Création des préférences de Captvty (notamment ajout de VLC de wine pour la lecture des vidéos en replay)
cat <<FIN > "$HOME/.captvty_V3/Captvty.settings"
<?xml version="1.0" encoding="utf-8"?>
<settings>
  <maxRateFactor>5</maxRateFactor>
  <playerPaths>C:/Program Files/VideoLAN/VLC/vlc.exe</playerPaths>
  <maxRateEnabled>False</maxRateEnabled>
  <remuxEnabled>True</remuxEnabled>
  <downloadLocation>Z:/$HOME/Vidéos/Captvty_V3</downloadLocation>
  <maxJobs>0</maxJobs>
  <remuxRecycleEnabled>False</remuxRecycleEnabled>
  <recordingPaddingEnd>0</recordingPaddingEnd>
  <bandwidth>0</bandwidth>
  <windowMetrics>0, 0, 0, 0</windowMetrics>
  <remuxFormats>mp4</remuxFormats>
  <maxJobsEnabled>False</maxJobsEnabled>
  <recordingPaddingStart>0</recordingPaddingStart>
  <recordingPaddingEnabled>False</recordingPaddingEnabled>
  <windowState></windowState>
</settings>
FIN

#Création du fichier desktop pour avoir un raccourci du logiciel dans le menu
test -d "$HOME/.local/share/applications" || mkdir "$HOME/.local/share/applications"
cat << FIN > "$HOME/.local/share/applications/Captvty_V3.desktop"
[Desktop Entry]
Comment[fr_FR]=
Comment=
Exec=env WINEPREFIX="$HOME/.wine_captvty_V3" wine $HOME/.captvty_V3/Captvty.exe
GenericName[fr_FR]=Regarder et enregistrer la tv
GenericName=Regarder et enregistrer la tv
Icon=$HOME/.icons/captvty-logo.png
Categories=AudioVideo;
MimeType=
Name[fr_FR]=Captvty V3
Name=Captvty V3
Path=$HOME
StartupNotify=true
Terminal=false
TerminalOptions=
Type=Application
X-DBUS-ServiceName=
X-DBUS-StartupType=
X-KDE-SubstituteUID=false
X-KDE-Username=
FIN
