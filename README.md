# Captvty Installateur Pour ArchLinux  
  
Il s'agit d'un script bash pour l'installation de [Captvty V3 (alpha)](https://v3.captvty.fr/) sous ArchLinux.  
Ce programme développé pour Windows permet de regarder la tv en direct ou de récupérer les programmes tv depuis le replay.  
Du coup, les scripts installent wine et configure des prefix wine pour faire tourner la version du logiciel choisit.  
Le logiciel s'installe dans le dossier utilisateur, du coup si vous voulez l'utiliser pour tous les utilisateurs de votre système, il faut l'exécuter sur chaque session.  
  
Le script crée des raccourcis dans votre menu d'application, quelque soit votre DE (kde, xfce, gnome, etc.).  
Le script génèrent les paramètres suivant : lecteur vlc (version wine) comme lecteur de média par défaut pour le visionnage de la tv et l'enregistrement des vidéos dans le dossier ~/Vidéos/Captvty_V3.  
  
Pour info, si jamais vous avez un message vous indiquant une mise à jour du logiciel, vous n'avez qu'à réxécuter le script et le logiciel sera automatiquement mis à jour.  
  
## Installation  
  
Pour installer Captvty V3 veuillez suivre les commandes suivantes:  
  
```bash
wget https://framagit.org/Paullux/captvty-script-installateur-pour-archlinux/-/raw/main/CaptvtyV3.sh
chmod +x CaptvtyV3.sh
./CaptvtyV3.sh
```  
  
  Une fois un script lancé, laissez le tourner (pas besoin d'intéraction supplémentaire), et une fois l'exécution du script terminée allez dans votre menu et Captvty sera présent.  
  
## Contributing  
  
Pour la création des scripts, j'ai eu l'aide de la communauté [Ubuntu-fr.org](https://forum.ubuntu-fr.org/viewtopic.php?id=2027322)


## Test Complet de Captvty V3 sous Ubuntu 18.10 avec Wine (fonctionnement identique sur Arch):

[![Watch the video](./ImageVideoYoutube.png)](https://youtu.be/a5CEMoGBehA)  
